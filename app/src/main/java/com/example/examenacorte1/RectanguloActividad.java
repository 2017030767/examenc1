package com.example.examenacorte1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActividad extends AppCompatActivity {
    private Rectangulo rectangulo;
    private TextView lblPersona;
    private TextView lblBase;
    private EditText txtBase;
    private TextView lblAltura;
    private EditText txtAltura;
    private TextView lblCalculoArea;
    private TextView lblCalArea;
    private TextView lblCalculoPerimetro;
    private TextView lblCalPerimetro;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo_actividad);

        lblPersona = (TextView) findViewById(R.id.lblPersona);
        lblBase = (TextView) findViewById(R.id.lblBase);
        txtBase = (EditText) findViewById(R.id.txtBase);
        lblAltura = (TextView) findViewById(R.id.lblAltura);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        lblCalculoArea = (TextView) findViewById(R.id.lblCalculoArea);
        lblCalArea = (TextView) findViewById(R.id.lblCalArea);
        lblCalculoPerimetro = (TextView) findViewById(R.id.lblCalculoPerimetro);
        lblCalPerimetro = (TextView) findViewById(R.id.lblCalPerimetro);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);


        Bundle datos = getIntent().getExtras();

        String persona = " " + datos.getString("persona");

        lblPersona.setText(persona);
        rectangulo = (Rectangulo) datos.getSerializable("rectangulo");

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String base = txtBase.getText().toString();
                String altura = txtAltura.getText().toString();
                if(base.matches("") || altura.matches("")){
                    Toast.makeText(RectanguloActividad.this, "Favor de llenar los campos", Toast.LENGTH_SHORT).show();
                } else {
                    rectangulo.setBase(Integer.parseInt(base));
                    rectangulo.setAltura(Integer.parseInt(altura));

                    lblCalArea.setText(String.valueOf(rectangulo.calcularCalcularArea()));
                    lblCalPerimetro.setText(String.valueOf(rectangulo.calcularPagPerimetro()));
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtAltura.setText("");
                txtBase.setText("");
                lblCalPerimetro.setText("");
                lblCalArea.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}

package com.example.examenacorte1;

import java.io.Serializable;

public class Rectangulo implements Serializable {
    private int base;
    private int altura;

    public Rectangulo() {
    }

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    //Llamado calcularCalcularArea porque asi venia en el diagrama de clases
    public float calcularCalcularArea(){
        return this.altura * this.base;
    }

    //Nombrado de esta manera porque viene asi en el diagrama de clases
    public float calcularPagPerimetro(){
        return (this.base * 2) + (this.altura * 2);
    }

}
